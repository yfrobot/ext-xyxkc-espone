# XYXKC 入门主控制板

![](./arduinoC/_images/featured.png)

---------------------------------------------------------

## 简介 Introduction

本扩展库为Mind+软件设计。

支持入门主控制板；主板基于ESP32-E芯片设计，板载复位按键、开关机按键、双可编程按键、双路电机驱动、电源供电系统（可充电）及多个防反接端口，适合培训机构及低龄用户使用。


## 相关链接 Links
* 本项目加载链接: https://gitee.com/yfrobot/ext-xyxkc-espone

* 产品购买链接: www.xyxedu.cn


## 积木列表 Blocks
![](./arduinoC/_images/blocks.png)


## 示例程序 Examples
![](./arduinoC/_images/example.png)


## 许可证 License
MIT


## 硬件支持列表 Hardware Support

主板型号                | 实时模式    | ArduinoC   | MicroPython    | 备注
------------------ | :----------: | :----------: | :---------: | -----
ESP32-E        |             |        √已测试      |             | 


## 更新日志 Release Note

* V0.0.2  电机驱动，按键，OLED显示，电压读取
* V0.0.1  电机驱动，Mind+V1.7.2 RC3.0版本软件测试



## 联系我们 Contact Us
* www.xyxedu.cn


## 其他扩展库 Other extension libraries
* www.xyxedu.cn


## 参考 Reference Resources