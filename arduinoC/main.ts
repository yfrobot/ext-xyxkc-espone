/** 
 * @file xyxkc
 * @brief xyxkc's ESPONE Mind+ library.
 * @n This is a MindPlus graphics programming extension for xyxkc's module.
 * 
 * @copyright    xyxkc,2023
 * @copyright    MIT Lesser General Public License
 * 
 * @author [email](xyxkc@qq.com)
 * @date  2023-04-10
*/
enum HIGHLOW {
    //% block="LOW"
    LOW,
    //% block="HIGH"
    HIGH
}

enum MWDIR {
    //% block="FORWARD"
    0,
    //% block="BACK"
    1,
    //% block="RIGHT"
    2,
    //% block="LEFT"
    3
}

enum MOTORNUM {
    //% block="M1"
    M1,
    //% block="M2"
    M2,
    //% block="MAll"
    MAll,
}

enum BTN {
    //% block="A"
    A,
    //% block="B"
    B,
}

enum CHINECE_FONT {
    //% block="u8g2_font_unifont_t_chinese2"
    u8g2_font_unifont_t_chinese2,
    //% block="u8g2_font_wqy12_t_gb2312"
    u8g2_font_wqy12_t_gb2312,
}

//% color="#1491a8" iconWidth=50 iconHeight=40
namespace espone {

    //% block="Motor M1 [SPEEDM1] M2 [SPEEDM2] " blockType="command"
    //% SPEEDM1.shadow="range"   SPEEDM1.params.min=-1023    SPEEDM1.params.max=1023    SPEEDM1.defl=512
    //% SPEEDM2.shadow="range"   SPEEDM2.params.min=-1023    SPEEDM2.params.max=1023    SPEEDM2.defl=512
    export function MotorDrive(parameter: any, block: any) {
        let speedM1 = parameter.SPEEDM1.code;
        let speedM2 = parameter.SPEEDM2.code;

        Generator.addInclude(`definemotorM1`, `PROGMEM void esponeM1Set(int m1Speed); // ESPONE电机1控制函数`)
        Generator.addInclude(`definemotorM1Fun`, `/*\n` +
            `   驱动电机M1函数, 参数:m1Speed - 1电机速度 (取值范围：-1023 ~ 1023) */\n` +
            `void esponeM1Set(int m1Speed) {\n` +
            `  if (m1Speed > 0) {\n` +
            `    digitalWrite(18, LOW);\n` +
            `  } else if (m1Speed < 0) {\n` +
            `    digitalWrite(18, HIGH);\n` +
            `  } \n` +
            `  analogWrite(19, abs(m1Speed));\n` +
            `}`
        );

        Generator.addInclude(`definemotorM2`, `PROGMEM void esponeM2Set(int m2Speed); // ESPONE电机2控制函数`)
        Generator.addInclude(`definemotorM2Fun`, `/*\n` +
            `   驱动电机M2函数, 参数:m2Speed - 2电机速度 (取值范围：-1023 ~ 1023) */\n` +
            `void esponeM2Set(int m2Speed) {\n` +
            `  if (m2Speed > 0) {\n` +
            `    digitalWrite(23, LOW);\n` +
            `  } else if (m2Speed < 0) {\n` +
            `    digitalWrite(23, HIGH);\n` +
            `  }\n` +
            `  analogWrite(16, abs(m2Speed));\n` +
            `}`
        );

        Generator.addCode(`esponeM1Set(${speedM1});`);
        Generator.addCode(`esponeM2Set(${speedM2});`);
    }

    
    //% block="Motor [MOTOR] SPEED [SPEED] " blockType="command"
    //% MOTOR.shadow="dropdown" MOTOR.options="MOTORNUM" MOTOR.defl="MOTORNUM.M1"
    //% SPEED.shadow="range"   SPEED.params.min=-1023    SPEED.params.max=1023    SPEED.defl=512
    export function MotorDrive_Single(parameter: any, block: any) {
        let motor = parameter.MOTOR.code;
        let speed = parameter.SPEED.code;

        Generator.addInclude(`definemotorM1`, `PROGMEM void esponeM1Set(int m1Speed); // ESPONE电机1控制函数`)
        Generator.addInclude(`definemotorM1Fun`, `/*\n` +
            `   驱动电机M1函数, 参数:m1Speed - 1电机速度 (取值范围：-1023 ~ 1023) */\n` +
            `void esponeM1Set(int m1Speed) {\n` +
            `  if (m1Speed > 0) {\n` +
            `    digitalWrite(18, LOW);\n` +
            `  } else if (m1Speed < 0) {\n` +
            `    digitalWrite(18, HIGH);\n` +
            `  } \n` +
            `  analogWrite(19, abs(m1Speed));\n` +
            `}`
        );

        Generator.addInclude(`definemotorM2`, `PROGMEM void esponeM2Set(int m2Speed); // ESPONE电机2控制函数`)
        Generator.addInclude(`definemotorM2Fun`, `/*\n` +
            `   驱动电机M2函数, 参数:m2Speed - 2电机速度 (取值范围：-1023 ~ 1023) */\n` +
            `void esponeM2Set(int m2Speed) {\n` +
            `  if (m2Speed > 0) {\n` +
            `    digitalWrite(23, LOW);\n` +
            `  } else if (m2Speed < 0) {\n` +
            `    digitalWrite(23, HIGH);\n` +
            `  }\n` +
            `  analogWrite(16, abs(m2Speed));\n` +
            `}`
        );

        if(motor == "M1"){
            Generator.addCode(`esponeM1Set(${speed});`);
        } else if (motor == "M2"){
            Generator.addCode(`esponeM2Set(${speed});`);
        } else {
            Generator.addCode(`esponeM1Set(${speed});`);
            Generator.addCode(`esponeM2Set(${speed});`);
        }
    }

    //% block="Motor [DIR] at [SPE] speed" blockType="command"
    //% DIR.shadow="dropdown" DIR.options="MWDIR" DIR.defl="MWDIR.FORWARD"
    //% SPE.shadow="range"   SPE.params.min=0    SPE.params.max=1023    SPE.defl=512
    export function MotorMove(parameter: any, block: any) {
        let dir = parameter.DIR.code;
        let spe = parameter.SPE.code;

        Generator.addInclude(`definemotorM1`, `PROGMEM void esponeM1Set(int m1Speed); // ESPONE电机1控制函数`)
        Generator.addInclude(`definemotorM1Fun`, `/*\n` +
            `   驱动电机M1函数, 参数:m1Speed - 1电机速度 (取值范围：-1023 ~ 1023) */\n` +
            `void esponeM1Set(int m1Speed) {\n` +
            `  if (m1Speed > 0) {\n` +
            `    digitalWrite(18, LOW);\n` +
            `  } else if (m1Speed < 0) {\n` +
            `    digitalWrite(18, HIGH);\n` +
            `  } \n` +
            `  analogWrite(19, abs(m1Speed));\n` +
            `}`
        );

        Generator.addInclude(`definemotorM2`, `PROGMEM void esponeM2Set(int m2Speed); // ESPONE电机2控制函数`)
        Generator.addInclude(`definemotorM2Fun`, `/*\n` +
            `   驱动电机M2函数, 参数:m2Speed - 2电机速度 (取值范围：-1023 ~ 1023) */\n` +
            `void esponeM2Set(int m2Speed) {\n` +
            `  if (m2Speed > 0) {\n` +
            `    digitalWrite(23, LOW);\n` +
            `  } else if (m2Speed < 0) {\n` +
            `    digitalWrite(23, HIGH);\n` +
            `  }\n` +
            `  analogWrite(16, abs(m2Speed));\n` +
            `}`
        );

        if (dir == `0`) {
            Generator.addCode(`esponeM1Set(${spe});`);
            Generator.addCode(`esponeM2Set(${spe});`);
        } else if (dir == `1`) {
            // Generator.addCode(`esponeMotorDrive(0-${spe},0-${spe});`);
            Generator.addCode(`esponeM1Set(0-${spe});`);
            Generator.addCode(`esponeM2Set(0-${spe});`);
        } else if (dir == `2`) {
            // Generator.addCode(`esponeMotorDrive(${spe},${spe}/2);`);
            Generator.addCode(`esponeM1Set(${spe});`);
            Generator.addCode(`esponeM2Set(${spe}/2);`);
        } else if (dir == `3`) {
            // Generator.addCode(`esponeMotorDrive(${spe}/2,${spe});`);
            Generator.addCode(`esponeM1Set(${spe}/2);`);
            Generator.addCode(`esponeM2Set(${spe});`);
        }
    }

    //% block="Motor [MOTOR] STOP" blockType="command"
    //% MOTOR.shadow="dropdown" MOTOR.options="MOTORNUM" MOTOR.defl="MOTORNUM.M1"
    export function MotorStop(parameter: any, block: any) {
        let motor = parameter.MOTOR.code;

        Generator.addInclude(`definemotorM1`, `PROGMEM void esponeM1Set(int m1Speed); // ESPONE电机1控制函数`)
        Generator.addInclude(`definemotorM1Fun`, `/*\n` +
            `   驱动电机M1函数, 参数:m1Speed - 1电机速度 (取值范围：-1023 ~ 1023) */\n` +
            `void esponeM1Set(int m1Speed) {\n` +
            `  if (m1Speed > 0) {\n` +
            `    digitalWrite(18, LOW);\n` +
            `  } else if (m1Speed < 0) {\n` +
            `    digitalWrite(18, HIGH);\n` +
            `  } \n` +
            `  analogWrite(19, abs(m1Speed));\n` +
            `}`
        );

        Generator.addInclude(`definemotorM2`, `PROGMEM void esponeM2Set(int m2Speed); // ESPONE电机2控制函数`)
        Generator.addInclude(`definemotorM2Fun`, `/*\n` +
            `   驱动电机M2函数, 参数:m2Speed - 2电机速度 (取值范围：-1023 ~ 1023) */\n` +
            `void esponeM2Set(int m2Speed) {\n` +
            `  if (m2Speed > 0) {\n` +
            `    digitalWrite(23, LOW);\n` +
            `  } else if (m2Speed < 0) {\n` +
            `    digitalWrite(23, HIGH);\n` +
            `  }\n` +
            `  analogWrite(16, abs(m2Speed));\n` +
            `}`
        );

        if(motor == "M1"){
            Generator.addCode(`esponeM1Set(0);`);
        } else if (motor == "M2"){
            Generator.addCode(`esponeM2Set(0);`);
        } else {
            Generator.addCode(`esponeM1Set(0);`);
            Generator.addCode(`esponeM2Set(0);`);
        }
    }

    //% block="Read button [BTN]" blockType="boolean"
    //% BTN.shadow="dropdown" BTN.options="BTN" MOTOR.defl="BTN.A"
    export function readBtn(parameter: any, block: any) {
        let btn = parameter.BTN.code;
        
        if(btn == "A"){
            Generator.addCode(`!digitalRead(34)`);
        } else if (btn == "B"){
            Generator.addCode(`!digitalRead(35)`);
        }
    }
    
    //% block="Read battery voltage (unit V)" blockType="reporter"
    export function readVBat(parameter: any, block: any) {
        Generator.addCode(`analogRead(36) * 6.6 / 4095`);
    }

        
    //% block="OLED 128*64 init I2C" blockType="command"
    export function oledinit(parameter: any, block: any) {
        let add = 0x3d;

        Generator.addInclude("U8g2lib", `#include <U8g2lib.h>`);
        
        Generator.addInclude("U8g2lib_object", "U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2_12864(U8G2_R0, /* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);  // High speed I2C")
        //Generator.addInclude("U8g2lib_object", "U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2_12864(U8G2_R0, /* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);    //Low spped I2C")

        // 其他所有字体：  https://github.com/olikraus/u8g2/wiki/fntlistallplain#u8g2-font-list
        // 其他中文字体: https://github.com/larryli/u8g2_wqy 
        Generator.addInclude(`defineOLED12864Init`, `PROGMEM void oled12864Begin(uint8_t addr, uint8_t dir); // OLED初始化函数`)
        Generator.addInclude(`defineOLED12864InitFun`, `\n` +
            `void oled12864Begin(uint8_t addr, uint8_t dir) {             // OLED初始化函数\n` +
            `  u8g2_12864.setI2CAddress(addr<<1);\n` +
            `  u8g2_12864.begin();\n` +
            `  u8g2_12864.enableUTF8Print();\n` +
            `  u8g2_12864.setFont(u8g2_font_wqy12_t_gb2312);\n` +
            `  u8g2_12864.setFontDirection(dir);\n` +
            `}`
        );

        Generator.addSetup("U8g2lib_begin_setup",`oled12864Begin(${add}, 0);`);
    }
            
    //% block="OLED 128*64 show text [TEXT] at coordinates X:[X] Y:[Y]" blockType="command"
    //% TEXT.shadow="string" TEXT.defl=xyxkc
    //% X.shadow="range"   X.params.min=0    X.params.max=127    X.defl=0
    //% Y.shadow="range"   Y.params.min=0    Y.params.max=63    Y.defl=0
    export function oledprint(parameter: any, block: any) {
        let text = parameter.TEXT.code;
        let x = parameter.X.code;
        let y = parameter.Y.code;

        Generator.addInclude(`defineOLED12864Pint`, `PROGMEM void oled12864Print(String text, uint8_t x, uint8_t y); // OLED显示函数`)
        Generator.addInclude(`defineOLED12864PintFun`, `\n` +
            `void oled12864Print(String text, uint8_t x, uint8_t y) {             // OLED显示函数\n` +
            `  u8g2_12864.setCursor(x, y);\n` +
            `  u8g2_12864.print(text);\n` +
            `}`
        );

        Generator.addCode(`oled12864Print(${text},${x},${y});`);
    }

    //% block="OLED 128*64 show" blockType="command"
    export function oledsendBuffer(parameter: any, block: any) {
        Generator.addCode(`u8g2_12864.sendBuffer();`);
    }

    //% block="OLED 128*64 clear" blockType="command"
    export function oledclear(parameter: any, block: any) {
        Generator.addCode(`u8g2_12864.clear();`);
    }
    
    //% block="OLED 128*64 set font [FONT]" blockType="command"
    //% FONT.shadow="dropdownRound" FONT.options="CHINECE_FONT" MOTOR.defl=CHINECE_FONT.u8g2_font_wqy12_t_gb2312
    export function oledSetFont(parameter: any, block: any) {
        let font = parameter.FONT.code;
        Generator.addCode(`u8g2_12864.setFont(${font});`);
    }
}
